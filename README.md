# Argo K3S Setup

Creates a single node k3s cluster with Argo Workflows installed.

## Prerequisites

- A fresh Linux distro (tested on Ubuntu).
- cURL installed.

## Deploy

```bash
bash install.sh
kcc default
```

- To run on a cloud provider, remove the k3s install from the script.

## Notes

This is not production ready.

- Single node cluster.
- Install using persistent storage / storage class, if required.
- Install using an Ingress, instead of LB (domain required).
- Argo server auth is disabled.
