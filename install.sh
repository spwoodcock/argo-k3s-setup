#/bin/bash

echo "------------------"
echo "Installing k3s"
echo "------------------"
curl -sfL https://get.k3s.io | sh -
echo "Modify K3S kube config to 777 - INSECURE"
sudo chmod 777 /etc/rancher/k3s/k3s.yaml
echo "Copy kube config to home dir"
mkdir -p ~/.kube
cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
echo "Revert permissions to 400"
sudo chmod 400 /etc/rancher/k3s/k3s.yaml ~/.kube/config

echo "------------------"
echo "Installing Helm"
echo "------------------"
cd $(mktemp -d)
curl -LO https://get.helm.sh/helm-v3.10.2-linux-amd64.tar.gz
tar -xvzf helm-v3.10.2-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
cd ~

echo "------------------"
echo "Installing Kubie"
echo "------------------"
cd $(mktemp -d)
curl -LO https://github.com/sbstp/kubie/releases/download/v0.19.1/kubie-linux-amd64
sudo mv kubie-linux-amd64 /usr/local/bin/kubie
sudo chmod +x /usr/local/bin/kubie
cd ~

echo "------------------"
echo "Installing Argo CLI"
echo "------------------"
cd $(mktemp -d)
curl -LO https://github.com/argoproj/argo-cd/releases/download/v2.5.3/argocd-linux-amd64
sudo mv argocd-linux-amd64 /usr/local/bin/argo
sudo chmod +x /usr/local/bin/argo
cd ~

echo "------------------"
echo "Adding aliases to .bashrc"
echo "------------------"
echo alias k='kubectl' >> ~/.bashrc
echo alias kcc="'kubie ctx'" >> ~/.bashrc
echo alias ns="'kubie ns'" >> ~/.bashrc
source ~/.bashrc

echo "------------------"
echo "Install Argo Workflow via Helm"
echo "------------------"
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm upgrade --install argo-workflows argo/argo-workflows \
  --namespace kube-system --values - <<EOF
server:
  serviceType: LoadBalancer
  extraArgs:
  - --auth-mode=server
EOF

echo "Access Argo Workflows Server on IP: $(kubectl --kubeconfig ~/.kube/config get services -n kube-system argo-workflows-server --output go-template='{{range.status.loadBalancer.ingress}}{{if .ip}}{{.ip}}{{end}}{{end}}:{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}')"

